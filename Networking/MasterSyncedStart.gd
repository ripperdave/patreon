extends Node2D

var ip = "127.0.0.1"
#var ip = "10.55.186.195"
var port = 233223
var host = null

var netId = 0

var player_scene = load("res://RigidPlayer.tscn")

var playerServer
var playerClient

var players = []


func _ready():
	
	get_tree().connect("network_peer_connected", self, "connected")
	get_tree().connect("server_disconnected", self, "finish_game")
	if (host_game(port)!= 0):
		join_game(ip, port)
		

var started = false

func _process(delta):
	
	if (is_network_master() and !started && Input.is_key_pressed(KEY_SPACE)):
		started = true
		rpc("initAll", players)


sync func initAll(inPlayers):
	for p in inPlayers:
		instPlayer(p)
		

func host_game(port):
	print("try host")
	host = NetworkedMultiplayerENet.new()
	var r = host.create_server(port)
	
	players.append(1)
	
	if (r!=0):
		print("Host problem!")
		return 1
		
	netId = host.get_unique_id()	
	get_tree().set_network_peer(host)
		
	print("Host ok")
	return 0


	
#client
func join_game(ip, port):
	print("try client")
	host = NetworkedMultiplayerENet.new()
	var r = host.create_client(ip, port)
	
	if (r!=0):
		print("Connect problem")
		return 1
		
	get_tree().set_network_peer(host)
	netId = host.get_unique_id()	
	print("Client ok")
	
	return 0
	
	
sync func instPlayer(id):
	
		
	var player = player_scene.instance()
	print("instancing id:"+String(id))
	player.set_network_master(id)
	
	if (id == 1):
		print("instancing server player")
		player.name = "serverPlayer"
		
		player.position = Vector2(30,30)
		player.modulate = Color(1,0,0)
		
	
		playerServer = player
	else:
		print("instancing client player")
		player.name = "clientPlayer"+String(id)
		player.modulate = Color(0,1,0)
		player.position = Vector2(100,100)
		
		playerClient = player
	
	
	add_child(player)
		



	
func finish_game(port):
	get_tree().set_network_peer(null)
	
func connected(id):
	
	if (!is_network_master()):
		return
	
	players.append(id)	
	


   

