extends RigidBody2D

var force = 10
const zero = Vector2(0,0)




func _ready():
	if (is_network_master()):
		var camera = find_node("Camera2D")
		camera.current = true
	

func _process(delta):

	if (!is_network_master()):
		return
		

	if (Input.is_key_pressed(KEY_UP)):
		apply_impulse(zero, Vector2(0, -force))
		return
	
	if (Input.is_key_pressed(KEY_DOWN)):
		apply_impulse(zero, Vector2(0, force))
		return
		
	if (Input.is_key_pressed(KEY_LEFT)):
		apply_impulse(zero, Vector2(-force,0))
		return
	
	if (Input.is_key_pressed(KEY_RIGHT)):
		apply_impulse(zero, Vector2(force,0))
	
		return


var savedTrans = null

slave func updateTrans(t):
	savedTrans = t

func _integrate_forces(state):
		
	if (is_network_master()):
		rpc_unreliable("updateTrans",state.transform)
	else:
		if (savedTrans != null):
			state.transform = savedTrans
